<?php

namespace Repository;

use PDO;

/**
 * Db repository
 */
class Db extends AbstractRepository
{
	/**
	 * @var $params
	 */
	protected $params = [
		'dsn'      => '',
		'username' => '',
		'password' => '',
		'options'  => [],
		'table'    => 'repository',
		'value'    => 'value',
	];

	/**
	 * @var PDO
	 */
	protected $connection;

	/**
	 * Create new Db repository
	 *
	 * @param array $params
	 */
	public function __construct($params)
	{
		$this->params     = array_merge($this->params, $params);
		$this->connection = new PDO($this->params['dsn'], $this->params['username'], $this->params['password'], $this->params['options']);

		$this->connection->query('AL')
	}

	/**
	 * Clear repository
	 */
	public function clear()
	{
		$this->connection->query()->ftruncate(0);
	}

	/**
	 * Add data
	 *
	 * @param $value
	 * @return $this
	 */
	public function add($value)
	{
		$this->file->fwrite($value.PHP_EOL);
	}

	/**
	 * @inheritdoc
	 */
	public function next()
	{
		$this->file->next();
	}

	/**
	 * @inheritdoc
	 */
	public function valid()
	{
		return $this->file->valid();
	}

	/**
	 * @inheritdoc
	 */
	public function current()
	{
		return $this->file->current();
	}

	/**
	 * @inheritdoc
	 */
	public function key()
	{
		return $this->file->key();
	}

	/**
	 * @inheritdoc
	 */
	public function rewind()
	{
		$this->file->rewind();
	}
}