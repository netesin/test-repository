<?php

namespace Repository;

/**
 * AbstractArray repository
 */
abstract class AbstractArray extends AbstractRepository
{
	/**
	 * @var array
	 */
	protected $array = [];

	/**
	 * Create new AbstractArray repository
	 *
	 * @param array $array
	 */
	public function __construct(array $array = [])
	{
		$this->array = $array;
	}

	/**
	 * Clear repository
	 */
	public function clear()
	{
		$this->array = [];
	}

	/**
	 * Add data
	 *
	 * @param $value
	 * @return $this
	 */
	public function add($value)
	{
		$this->array[] = $value;
	}

	/**
	 * @inheritdoc
	 */
	public function next()
	{
		next($this->array);
	}

	/**
	 * @inheritdoc
	 */
	public function valid()
	{
		return array_key_exists(key($this->array), $this->array);
	}

	/**
	 * @inheritdoc
	 */
	public function current()
	{
		return current($this->array);
	}

	/**
	 * @inheritdoc
	 */
	public function key()
	{
		return key($this->array);
	}

	/**
	 * @inheritdoc
	 */
	public function rewind()
	{
		reset($this->array);
	}
}