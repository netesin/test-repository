<?php

namespace Repository;

/**
 * AbstractArray repository
 */
class PhpArray extends AbstractArray
{
	/**
	 * Create new PhpArray repository
	 *
	 * @param array $array
	 */
	public function __construct(array $array = [])
	{
		parent::__construct($array);
	}

}