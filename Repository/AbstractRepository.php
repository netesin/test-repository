<?php

namespace Repository;

/**
 * AbstractRepository repository
 */
abstract class AbstractRepository implements RepositoryInterface
{
	/**
	 * Export to repository
	 *
	 * @param RepositoryInterface $repository
	 */
	public function export(RepositoryInterface $repository)
	{
		$repository->import($this);
	}

	/**
	 * Import from repository
	 *
	 * @param RepositoryInterface $repository
	 */
	public function import(RepositoryInterface $repository)
	{
		$this->clear();

		foreach ($repository as $value) {
			$this->add($value);
		}

	}
}