<?php

namespace Repository;

use Iterator;

/**
 * Repository interface
 */
interface RepositoryInterface extends Iterator
{

	/**
	 * Import from repository
	 *
	 * @param RepositoryInterface $repository
	 */
	public function import(RepositoryInterface $repository);

	/**
	 * Export to repository
	 *
	 * @param RepositoryInterface $repository
	 */
	public function export(RepositoryInterface $repository);

	/**
	 * Clear repository
	 */
	public function clear();

	/**
	 * Add data
	 *
	 * @param $value
	 * @return $this
	 */
	public function add($value);

}