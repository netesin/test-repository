<?php

namespace Repository;

use SplFileObject;

/**
 * File repository
 */
class File extends AbstractRepository
{
	/**
	 * @var string
	 */
	protected $path;

	/**
	 * @var SplFileObject
	 */
	protected $file;

	/**
	 * Create new PhpArray repository
	 *
	 * @param string $path
	 */
	public function __construct($path)
	{
		$this->path = $path;
		$this->file = new SplFileObject($path, 'r+');
	}

	/**
	 * Clear repository
	 */
	public function clear()
	{
		$this->file->ftruncate(0);
	}

	/**
	 * Add data
	 *
	 * @param $value
	 * @return $this
	 */
	public function add($value)
	{
		$this->file->fwrite($value.PHP_EOL);
	}

	/**
	 * @inheritdoc
	 */
	public function next()
	{
		$this->file->next();
	}

	/**
	 * @inheritdoc
	 */
	public function valid()
	{
		return $this->file->valid();
	}

	/**
	 * @inheritdoc
	 */
	public function current()
	{
		return $this->file->current();
	}

	/**
	 * @inheritdoc
	 */
	public function key()
	{
		return $this->file->key();
	}

	/**
	 * @inheritdoc
	 */
	public function rewind()
	{
		$this->file->rewind();
	}
}