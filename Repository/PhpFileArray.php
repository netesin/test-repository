<?php

namespace Repository;

/**
 * PhpFileArray repository
 */
class PhpFileArray extends AbstractArray
{
	/**
	 * @var string
	 */
	protected $path;

	/**
	 * Create new PhpArray repository
	 *
	 * @param string $path
	 */
	public function __construct($path)
	{
		$this->path = $path;
		$array      = [];

		if (file_exists($path)) {
			$array = include $path;
		}

		parent::__construct((array)$array);
	}

	/**
	 * Import from repository
	 *
	 * @param RepositoryInterface $repository
	 */
	public function import(RepositoryInterface $repository)
	{
		parent::import($repository);

		file_put_contents($this->path, '<?php return '.var_export($this->array, true).';');

	}
}