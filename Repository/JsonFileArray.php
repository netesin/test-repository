<?php

namespace Repository;

/**
 * JsonFileArray repository
 */
class JsonFileArray extends AbstractArray
{
	/**
	 * @var string
	 */
	protected $path;

	/**
	 * Create new PhpArray repository
	 *
	 * @param string $path
	 */
	public function __construct($path)
	{
		$this->path = $path;
		$array      = [];

		if (file_exists($path)) {
			$array = json_decode($path, true);
		}

		parent::__construct((array)$array);
	}

	/**
	 * Import from repository
	 *
	 * @param RepositoryInterface $repository
	 */
	public function import(RepositoryInterface $repository)
	{
		parent::import($repository);

		file_put_contents($this->path, json_encode($this->array));

	}
}